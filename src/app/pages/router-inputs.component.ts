import { CommonModule, NgIf } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  standalone: true,
  imports: [],
  template: ` <div>countId: {{ countId }}</div> `,
})
export default class RequiredInputsComponent {
  @Input() public countId!: string;
}
